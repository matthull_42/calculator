class Calculator
  constructor: ->
    @parser = new Parser
    
  # Takes an expression such as '1 + 3 * 3'
  # Returns the numeric result of calculation
  # Only supports +, -, *, % operators
  # No parentheses support
  calculate: (expr) ->
    tokens = @parser.tokenize expr
    
    # Handle each priority 1 operator
    # Handle each priority 2 operator
    # Handle each priority 3 operator
    
class Parser
  tokenize: (str) ->
    tokens = str.split /\s*/g
    tokens.map (t) =>
        if Operator.isOperator(t)
            new Operator t
        else unless isNaN(t)
            t
        else
            throw "#{expr} should only contain +-*% and numbers"

class Operator
    constructor: (token) ->
        unless Operator.isOperator(token)
            throw "#{token} is not a valid operator"
        @token = token
        
    @isOperator: (char) ->
        Operator.validOperators().indexOf(char) != -1
    @validOperators: ->
        ['%', '+', '-', '*', '/']

### Integration Tests ###

#calculator = new Calculator
#expression = '1 + 2 + 3'
#expected = 6
#actual = calculator.calculate expression

#console.log "Calculator says #{expression} = #{actual}"
#console.log "Expected is #{expected}"
#if expected == actual
#    console.log 'Calculation correct'
#else
#    console.log 'Calculation INCORRECT'
